#include "sync_dictionary.hpp"
#include "tools.hpp"

sync_dictionary::sync_dictionary(const
                                 std::initializer_list<std::string>& init)
{
  root = new TrieNode();
  for (auto it = init.begin(); it != init.end(); ++it)
    insert(*it);
}


void sync_dictionary::init(const std::vector<std::string>& word_list)
{
  root = new TrieNode();
  for (auto str : word_list)
    insert(str);
}

void sync_dictionary::insert(const std::string& w)
{

  TrieNode *tmp = root;

  for (unsigned i = 0; i < w.size(); ++i)
  {
    const char c = w.at(i);

    if (tmp->children_.find(c) == tmp->children_.end())
      tmp->children_.insert({c,new TrieNode(tmp)});

    tmp = std::get<1>(*(tmp->children_.find(c)));
  }

  tmp->m_.lock();
  tmp->isEndOfWord_ = true;
  tmp->m_.unlock();
}

void sync_dictionary::erase(const std::string& w)
{

  TrieNode *tmp = root;
  for (unsigned i = 0; i < w.size(); ++i)
  {
    const char c = w.at(i);
    if (tmp->children_.find(c) == tmp->children_.end())
      return;
    else
      tmp = std::get<1>(*(tmp->children_.find(c)));
  }

  tmp->m_.lock();  
  tmp->isEndOfWord_ = false;
  tmp->m_.unlock();  

  // remove useless nodes
  for (int i = w.size() - 1; i >= 0; i--)
  {
    const char c = w.at(i);
    auto daddy = tmp->parent_;

    if (!tmp->isEndOfWord_ && tmp->children_.size() == 0)
    {
      tmp->m_.lock();
      daddy->m_.lock();
      daddy->children_.unsafe_erase(c);
      tmp->m_.unlock();
      daddy->m_.unlock();
      delete(tmp);
      tmp = daddy;
    }
    else
      break;
  }
}

result_t search_closest(const std::string& ref_str, TrieNode *node,
                        std::string current_str, result_t& res)
{
  if (node->isEndOfWord_)
  {
    int dist = levenshtein(current_str, ref_str);
    if (dist < res.second)
    {
      res = result_t{current_str, dist};
    }
  }

  for (auto child : node->children_)
  {
    search_closest(ref_str, child.second, current_str + child.first, res);
  }

  return res;
}

result_t sync_dictionary::search(const std::string& w) const
{

  TrieNode *tmp = root;
  int dist = 0;
  std::string res = "";
  for (unsigned i = 0; i < w.size(); ++i)
  {
    const char c = w.at(i);
    if (tmp->children_.find(c) == tmp-> children_.end())
    {
      tmp = root;
      std::string str;
      result_t res = result_t{str, levenshtein(str, w)};
      return search_closest(w, tmp, str, res);
    }
    else
    {
      res += c;
      tmp = std::get<1>(*(tmp->children_.find(c)));
    }
  }
  return std::make_pair(res, dist);
}
