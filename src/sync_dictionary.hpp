#pragma once

#include <string>

#include "trie.hpp"
#include "IDictionary.hpp"

class sync_dictionary : public IDictionary
{
public:
  sync_dictionary() = default;

  // only to test with naive_async_dictionary
  sync_dictionary(const std::initializer_list<std::string>& init);
  template <class Iterator>
  sync_dictionary(Iterator begin, Iterator end);

  void init(const std::vector<std::string>& word_list) final;


  result_t      search(const std::string& w) const final;
  void          insert(const std::string& w) final;
  void          erase(const std::string& w) final;

private:
  TrieNode *root;
};

template <class Iterator>
sync_dictionary::sync_dictionary(Iterator begin, Iterator end)
{
  root = new TrieNode();

  for (auto it = begin; it != end; ++it)
    insert(*it);
}