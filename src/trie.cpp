#include "trie.hpp"

TrieNode::TrieNode()
{
  parent_ = nullptr;
  isEndOfWord_ = false;
}

bool TrieNode::getIsEndOfWord()
{
  return isEndOfWord_; 
}

tbb::concurrent_unordered_map<char, TrieNode*> TrieNode::getChildren()
{
  return children_; 
}

TrieNode::TrieNode(TrieNode* parent)
{
  parent_ = parent;
  isEndOfWord_ = false;
}
