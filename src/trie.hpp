#pragma once
#include <unordered_map>
#include <mutex>
#include <string>
#include "tbb/concurrent_unordered_map.h"

class TrieNode
{
public:
  TrieNode();
  TrieNode(TrieNode* parent);
  ~TrieNode()
  {}

  bool getIsEndOfWord();
  tbb::concurrent_unordered_map<char, TrieNode*> getChildren();

  mutable std::mutex m_;

  TrieNode* parent_;
  tbb::concurrent_unordered_map<char, TrieNode*> children_;
  bool isEndOfWord_;
};
