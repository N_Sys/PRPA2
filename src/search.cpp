#include "search.hpp"

result_t Search::operator()(const std::string& ref_str, TrieNode *node,
                         std::string current_str, result_t& res)
    {
    if (node->isEndOfWord_)
      {
        int dist = levenshtein(current_str, ref_str);
        if (dist < res.second)
        {
          res = result_t{current_str, dist};
        }
      }

      for (auto child : node->children_)
      {
        this->operator()(ref_str, child.second, current_str + child.first, res);
      }

      return res;
    }
