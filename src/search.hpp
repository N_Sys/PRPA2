#pragma once
#include "tools.hpp"
#include "async_dictionary.hpp"

class Search
{
  public :
    Search() = default;
    result_t operator()(const std::string& ref_str, TrieNode *node,
                       std::string current_str, result_t& res);
};
