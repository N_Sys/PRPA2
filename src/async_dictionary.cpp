#include "async_dictionary.hpp"
#include "tools.hpp"
#include "search.hpp"

#include <future>
#include <string>
#include <iostream>
#include <algorithm>

async_dictionary::async_dictionary(const std::initializer_list<std::string>& init)
{
  root = new TrieNode();
  for (auto it = init.begin(); it != init.end(); ++it)
       insert(*it);
}

void async_dictionary::init(const std::vector<std::string>& word_list)
{
 root = new TrieNode();
 for (auto str : word_list)
    insert(str);
}

  result_t search_test(const std::string& ref_str,TrieNode* node, std::string current_str,const result_t& res)
      {
        result_t next_res = res;
        if (node->getIsEndOfWord())
        {

          int dist = levenshtein(current_str, ref_str);
          if (dist < res.second)
          {
            next_res = result_t{current_str, dist};
          }
        }
        for (auto child : node->children_)
        {
          const result_t& next = next_res;
          search_test(ref_str, child.second, current_str + child.first, next);
        }
        return next_res;
      }

std::future<result_t> async_dictionary::search(const std::string& w) const
{
  std::promise<result_t> result;
  TrieNode *tmp = root;
  int dist = 0;
  std::string str;
  Search s;
  for (unsigned i = 0; i < w.size(); ++i)
  {
    const char c = w.at(i);
    if (tmp->children_.find(c) == tmp-> children_.end())
    {
      tmp = root;
      str = "";
      result_t res = result_t{str, w.size()};

      return std::async(std::launch::async,search_test, w, tmp, str, res);
    }
    else
    {
      str += c;
      tmp = std::get<1>(*(tmp->children_.find(c)));
    }
  }
  result.set_value(std::make_pair(str, dist));
  return result.get_future();
}

std::future<void> async_dictionary::insert(const std::string& w)
{
   TrieNode *tmp = root;

  for (unsigned i = 0; i < w.size(); ++i)
  {
    std::lock_guard l(m);
    const char c = w.at(i);

    if (tmp->children_.find(c) == tmp->children_.end())
      tmp->children_.insert({c,new TrieNode(tmp)});

    tmp = std::get<1>(*(tmp->children_.find(c)));
  }

  tmp->isEndOfWord_ = true;
  return std::future<void>();
}

std::future<void> async_dictionary::erase(const std::string& w)
{
  TrieNode *tmp = root;
  for (unsigned i = 0; i < w.size(); ++i)
  {
    std::lock_guard l(m);
    const char c = w.at(i);
    if (tmp->children_.find(c) == tmp->children_.end())
      return std::future<void>();
    else
      tmp = std::get<1>(*(tmp->children_.find(c)));
  }

  tmp->isEndOfWord_ = false;

  // remove useless nodes
  for (int i = w.size() - 1; i >= 0; i--)
  {
    std::lock_guard l(m);
    const char c = w.at(i);
    auto daddy = tmp->parent_;

    if (!tmp->isEndOfWord_ && tmp->children_.size() == 0)
    {
      daddy->children_.unsafe_erase(c);
      delete(tmp);
      tmp = daddy;
    }
    else
      break;
  }
  return std::future<void>();
}
