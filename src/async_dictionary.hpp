#pragma once
#include <future>
#include "IAsyncDictionary.hpp"
#include "sync_dictionary.hpp"

class async_dictionary : public IAsyncDictionary
{
  public:
    async_dictionary() = default;
    async_dictionary(const std::initializer_list<std::string>& init);
    template <class Iterator>
    async_dictionary(Iterator begin, Iterator end);
    void init(const std::vector<std::string>& word_list) final;

    std::future<result_t> search(const std::string& w) const;
    std::future<void> insert(const std::string& w);
    std::future<void> erase(const std::string& w);

  private:
    TrieNode *root;
    mutable std::mutex m;
};

template <class Iterator>
async_dictionary::async_dictionary(Iterator begin, Iterator end)
{
  root = new TrieNode();

  for (auto it = begin; it != end; ++it)
    insert(*it);
}
